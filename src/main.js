// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import * as Tonal from 'tonal';
import * as Key from 'tonal-key';
import * as Abc from 'tonal-abc-notation';
import * as Tone from 'tone';
import _ from 'lodash';

_.set(Tonal, 'Key', Key);
_.set(Tonal, 'Abc', Abc);

Object.defineProperty(Vue.prototype, '$t', { value: Tonal });
Object.defineProperty(Vue.prototype, '$tone', { value: Tone });

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
});
