# chord-progression

This project is a web-based application designed to assist musicians, composers, and music enthusiasts in creating and exploring chord progressions. With this tool, you can choose a scale, a mode, and pick chords to play sounds from them, allowing you to experiment and create unique musical compositions.

- **Scale Selection**: Choose from a variety of musical scales, including major, minor, pentatonic, blues, and more. This feature provides a solid foundation for your chord progressions and ensures that the selected chords stay within the chosen scale.
- **Mode Selection**: Explore different musical modes to add depth and variety to your chord progressions. Modes such as Ionian, Dorian, Phrygian, Lydian, Mixolydian, Aeolian, and Locrian offer distinct tonalities and can help you achieve various musical moods.
- **Chord Playback**: Select chords from the available options and play their sounds directly within the application. This feature allows you to audition the chords and experiment with different combinations to create harmonically pleasing progressions.

![Demo](./demo.mp4)

---

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
